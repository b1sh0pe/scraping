const MongoConnect = require("./mongo-connect");

async function deleteAll() {
  const client = await MongoConnect.open();
  const db = client.db("SCRAPING");

  //const news = db.collection("news");
  const news_status = db.collection("news_status");

  return news_status.deleteMany({});
}

deleteAll().then(
  () => {
    process.exit(0);
  }
);