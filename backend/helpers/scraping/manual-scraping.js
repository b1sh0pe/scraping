const scrapService = require("../../services/scrapService");
const amqp = require("amqplib/callback_api");

amqp.connect("amqp://localhost", function (connectionError, connection) {
  if (connectionError) {
    throw connectionError;
  }

  connection.createChannel(function (channelError, channel) {
    if (channelError) {
      throw channelError;
    }

    const queueName = "scraping";
    channel.assertQueue(queueName, {
      durable: false
    });

    Promise.all([
      scrapService.getArticles("https://www.nytimes.com/section/business/media"),
      scrapService.getArticles("https://www.nytimes.com/section/technology"),
      scrapService.getArticles("https://www.nytimes.com/section/business/economy")
    ]).then(
      articles => {
        const resultData = {
          MEDIA: articles[0],
          TECHNOLOGY: articles[1],
          ECONOMY: articles[2]
        };

        const msg = JSON.stringify(resultData);

        channel.sendToQueue(queueName, Buffer.from(msg));

        setTimeout(() => {
          connection.close();
          process.exit(0);
        }, 500);
      }
    );
  });
});