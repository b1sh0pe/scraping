/*global console*/
/* eslint-disable no-console */
const amqp = require("amqplib/callback_api");
const mongoConnect = require("../../mongo-connect");

mongoConnect.open().then(
  (client) => {
    if (!client) {
      console.log("NO CONNECTION");
      return;
    }

    const db = client.db("SCRAPING");

    function exitHandler() {
      client.close();
      process.exit(0);
    }

    //do something when app is closing
    process.on("exit", exitHandler.bind(null, null));

    //catches ctrl+c event
    process.on("SIGINT", exitHandler.bind(null, null));

    amqp.connect("amqp://localhost", function (connectionError, connection) {
      if (connectionError) {
        throw connectionError;
      }

      connection.createChannel(function (channelError, channel) {
        if (channelError) {
          db.close();
          throw channelError;
        }

        const queue = "scraping";
        channel.assertQueue(queue, {
          durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
        channel.consume(queue, function (msg) {
          const content = msg.content.toString();
          const jsonData = JSON.parse(content);
          console.log(" [x] Received %s", JSON.stringify(jsonData, null, 4));

          const data = Object.keys(jsonData).reduce((accumulator, currVal) => {
            const newAccumulator = accumulator.concat(jsonData[currVal].map(
              elem => {
                return { ...elem, type: currVal };
              }
            ));
            return newAccumulator;
          }, []);

          const news = db.collection("news");

          news.find().toArray().then(
            foundNews => {
              const filteredData = data.filter(dataElem => !foundNews.some(elem => elem.link === dataElem.url));
              if (!filteredData.length) {
                return;
              }

              news.insertMany(filteredData).then(
                () => {
                  console.log("Successfully saved portion of news!!!");
                  mongoConnect.close(client);
                }
              ).catch(err => {
                console.log(err);
              });
            }
          ).catch(err => {
            console.log(err);
          });

        }, {
          noAck: true
        });
      });
    }
    );
  });