const fs = require("fs");
const puppeteer = require("puppeteer");
const util = require("util");

const rootPath = "../";
const config = require(rootPath + "config");
const logger = require(rootPath + "helpers/logger");
const utils = require(rootPath + "helpers/utils");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const tempDirectory = config.projectDir + "/" + config.tempDir;
const browserOptions = {
  args: [
    "--no-sandbox",
    "--disable-setuid-sandbox",
    "--disable-notifications",
    "--window-size=1600,900",
    "--lang=en_US"
  ],
  headless: true
};

const agents = [
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:34.0) Gecko/20100101 Firefox/34.0",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
];

function runBrowser(options) {
  return puppeteer.launch(Object.assign(browserOptions, options));
}

async function createPage(browser, cookiesFile) {
  logger.log("Creating page...");
  const page = await browser.newPage();
  await page.setUserAgent(agents[Math.floor(Math.random() * agents.length)]);
  await page.setViewport({width: 1600, height: 900});
  await page.setExtraHTTPHeaders({"Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8"});
  if (cookiesFile) {
    await loadCookies(page, cookiesFile);
  }
  logger.log("Page created.");
  return page;
}

async function goTo(page, url, options = {}) {
  logger.log(`Going to ${url}...`);

  options.waitUntil = options.waitUntil || "networkidle2";
  options.ignoreDestination = options.ignoreDestination !== undefined || false;
  options.timeout = options.timeout !== undefined || 60000;

  let again = true;
  while (again) {
    try {
      await page.goto(url, options);
      again = false;
    } catch (e) {
      if (e.message.indexOf("Navigation Timeout Exceeded") != -1) {
        logger.log("goTo timeout!");
      } else {
        throw e;
      }
    }
  }

  if (!options.ignoreDestination && page.url() != url) {
    throw Error("The current page is not the destination page. Page url: " + page.url());
  }
}

async function scrollPage(page, selector, xPosition = 1) {
  logger.log("Scrolling the page...");
  let again = true;
  while (again) {
    await page.evaluate("window.scrollTo(0, document.body.scrollHeight * " + xPosition + ")");
    try {
      await page.waitForSelector(selector, {timeout: 1000});
      return;
    } catch (e) {
      xPosition = xPosition >= 1 ? 0 : xPosition + 0.1;
      logger.log("Scrolling again to " + xPosition + "...");
      if (xPosition === 1) {
        again = false;
      }
    }
  }
}

async function infiniteScroll(page, action) {
  let oldScrollPos = 0;
  let newScrollPos = 0;
  let result = await action();
  while (!result) {
    logger.log("Scrolling the page...");
    newScrollPos = await page.evaluate(() => {
      window.scrollBy(0, document.body.scrollHeight);
      return document.body.scrollHeight;
    });
    if (oldScrollPos == newScrollPos) {
      break;
    }
    oldScrollPos = newScrollPos;
    result = await action();
  }
  return result;
}

async function click(page, element, timeout = 30) {
  logger.log("Clicking on element...");

  let again = true;
  while (again) {
    try {
      await page.evaluate((el) => {
        el.click();
      }, element);
      await page.waitForNavigation({timeout: timeout});
      again = false;
    } catch (e) {
      if (e.message.indexOf("Navigation Timeout Exceeded") !== -1) {
        logger.error("click() timeout !");
        await reloadPage(page);
      } else {
        throw e;
      }
    }
  }
}

async function reloadPage(page, timeout = 30, attempts = 5) {
  logger.log("Reloading page...");

  for (let i = 0; i < attempts; ++i) {
    try {
      await page.reload({timeout: timeout});
      return;
    } catch (e) {
      if (e.message.indexOf("Navigation Timeout Exceeded") !== -1) {
        logger.error("Page reloading timeout!");
      } else {
        throw e;
      }
    }
  }

  throw Error("The internet connection seems lost because the page cannot be reloaded.");
}

async function loadCookies(page, cookiesFile) {
  logger.log("Loading cookies...");
  let cookies;
  try {
    cookies = JSON.parse(await readFile(cookiesFile, "utf-8"));
  } catch (e) {
    createCookiesFilePath(cookiesFile);
    await writeFile(cookiesFile, "[]");
    logger.log("Empty cookies file created.");
    return;
  }
  await page.setCookie(...cookies);
  logger.log("Cookies loaded.");
}

async function saveCookies(page, cookiesFile) {
  logger.log("Saving cookies...");
  const cookies = JSON.stringify(await page.cookies());
  createCookiesFilePath(cookiesFile);
  await writeFile(cookiesFile, cookies);
  logger.log("Cookies saved.");
}

async function deleteCookiesFile(cookiesFile) {
  const fileExists = await fs.exists(cookiesFile);
  if (!fileExists) {
    logger.log("Cookies file does not exist.");
    return;
  }
  await fs.unlink(cookiesFile);
  logger.log("Cookies file deleted.");
}

function value(page, selector, value) {
  return page.evaluate(
    (selector, value) => {
      var elt = document.querySelector(selector);
      if (value !== undefined) {
        elt.value = value;
      }
      return elt.value;
    },
    selector,
    value
  );
}

async function attribute(page, selector, attribute, value) {
  return await page.evaluate(
    (selector, attribute, value) => {
      var elt = document.querySelector(selector);
      if (value !== undefined) {
        elt[attribute] = value;
      }
      return elt[attribute];
    },
    selector,
    attribute,
    value
  );
}

function createCookiesFilePath(filePath) {
  let cookiesFilePath = filePath.split("/");
  cookiesFilePath.pop();
  cookiesFilePath = cookiesFilePath.join("/");
  if (!fs.existsSync(cookiesFilePath)) {
    utils.createDirectoryRecursive(cookiesFilePath);
  }
}

async function doPageSnapshot(page) {
  try {
    const uniqueFileName = tempDirectory + "/person_profile_screenshot_" + new Date().getTime() + "_" + Math.random() * 1000;
    await page.screenshot({
      path: uniqueFileName + ".png"
    });
    const htmlContent = await page.content();
    utils.writeDataToFileSync(uniqueFileName + ".html", htmlContent);
    logger.info("[puppeteerUtils] Generated page screenshot: '" + uniqueFileName + ".png'");
    logger.info("[puppeteerUtils] Page html content saved as html file: '" + uniqueFileName + ".html'");
  } catch (error) {
    logger.error("[LinkedInScrape] Error in doPageSnapshot method. Cannot prepare page screenshot.", error);
  }
}

module.exports = {
  runBrowser: runBrowser,
  createPage: createPage,
  goTo: goTo,
  scrollPage: scrollPage,
  infiniteScroll: infiniteScroll,
  click: click,
  reloadPage: reloadPage,
  loadCookies: loadCookies,
  saveCookies: saveCookies,
  deleteCookiesFile: deleteCookiesFile,
  value: value,
  attribute: attribute,
  doPageSnapshot: doPageSnapshot
};
