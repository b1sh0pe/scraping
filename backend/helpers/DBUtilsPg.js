const pg = require("pg");//or .native but there were some problems
const yaml = require("js-yaml");
const fs = require("fs");

const rootPath = "../";
const logger = require(rootPath + "helpers/logger");
const config = require(rootPath + "config");

//let queryIndex = 1;//temp

function createPgPool(postgresConfig) {
  const pool = new pg.Pool(postgresConfig);
  pool.on("error", function (error, client) {
    // if an error is encountered by a client while it sits idle in the pool
    // the pool itself will emit an error event with both the error and
    // the client which emitted the original error
    // this is a rare occurrence but can happen if there is a network partition
    // between your application and the database, the database restarts, etc.
    // and so you might want to handle it and at least log it out
    let clientConnectionDetails = "";
    try {
      clientConnectionDetails = JSON.stringify(client, null, 2);
    } catch (err) {
      clientConnectionDetails = client;
    }
    logger.error("[DBUtilsPg] Idle client error " + error.message + ".\n Error stack: " + error.stack + ".");
    logger.error("[DBUtilsPg] Idle client connection error details" + clientConnectionDetails);
  });

  // Extend pool object with new execute query method
  pool.execute = function (query, params, rowMode) {
    return new Promise(function (resolve, reject) {
      // to run a query we can acquire a client from the pool,
      // run a query on the client, and then return the client to the pool
      pool.connect(function (error, client, done) {
        if (error) {
          reject(error);
          logger.error("[DBUtilsPg] Pool connect error in execute.", error);
          return false;
        }
        client.query({
          rowMode: rowMode || null,//'array',
          text: query
        }, params, function (err, results) {
          done();// to release the client back to the pool
          if (err) {
            logger.error("[DBUtilsPg] Error in execute method. Query: " + query);
            logger.error("[DBUtilsPg] Error in execute method: ", err);
            logger.error("[DBUtilsPg] Error in execute method. Detail: " + err.detail);
            logger.error("[DBUtilsPg] Error in execute method. More details: " + JSON.stringify(err));
            logger.error("[DBUtilsPg] Error in execute method. Params: " + JSON.stringify(params));
            reject(new Error("Database Error"));
            return false;
          }
          resolve(results);
        });
      });
    });
  };

  return pool;
}

const pool = createPgPool(config.postgres);

function getNamedQuery(fileName, queryName) {
  var namedQueries = "",
    query = "";

  // @todo to think read queries file in memory for better performance
  // for now it is ok for quick reload queries without server restart
  try {
    namedQueries = yaml.safeLoad(fs.readFileSync(config.projectDir + "/sql/pg/" + fileName, "utf8"));
    query = namedQueries[queryName];
    if (!query) {
      logger.error("[DBUtilsPg] Error in getNamedQuery method. Cannot find query with name `" + queryName + "`.");
      return "";
    }
    return query;
  } catch (error) {
    logger.error("[DBUtilsPg] Error in getNamedQuery method. Cannot get query with name `" + queryName + "`.", error);
    return "";
  }
}

function executeQueryRaw(query, params, rowMode) {
  //var index = queryIndex;
  //queryIndex++;
  //logger.trace("executeQueryRaw running queryIndex: " + index);
  //logger.trace("executeQueryRaw before running query: " + query);
  return pool.execute(query, params, rowMode).then(
    function (/*results*/) {
      //logger.trace("executeQueryRaw finished queryIndex: " + index);
      return true;
    }
  );
}

function executeQueryRawRowsFetch(query, params, rowMode) {
  //var index = queryIndex;
  //queryIndex++;
  //logger.trace("executeQueryRawRowsFetch running queryIndex: " + index);
  //logger.trace("executeQueryRaw before running query: " + query);
  return pool.execute(query, params, rowMode).then(
    function (results) {
      //logger.trace("executeQueryRawRowsFetch finished queryIndex: " + index);
      return results.rows;
    }
  );
}

function executeQueryRawInsert(query, params, rowMode) {
  //var index = queryIndex;
  //queryIndex++;
  //logger.trace("executeQueryRawInsert running queryIndex: " + index);
  //logger.trace("executeQueryRawInsert before running query: " + query);
  return pool.execute(query, params, rowMode).then(
    function (results) {
      //logger.trace("executeQueryRawInsert finished queryIndex: " + index);
      return results.rowCount;
    }
  );
}



//preffered way
function executeQuery(fileName, queryOrQueryName, params, rowMode) {
  let query = "";

  if (fileName === null) {
    query = queryOrQueryName;
  } else {
    query = getNamedQuery(fileName, queryOrQueryName);
  }
  //todo parametrize queries
  return pool.execute(query, params, rowMode);
}

function executeOneRowUpdate(fileName, query, params, rowMode) {
  return executeQuery(fileName, query, params, rowMode).then(
    function (results) {
      var updatedRows = results.rowCount;

      if (updatedRows !== 1) {
        throw new Error("[DBUtilsPg] Error in executeOneRowUpdate method. Wrong one row modification query. Rows affected: " + updatedRows);
      }
      return true;
    }
  ).catch(
    function (error) {
      logger.error("[DBUtilsPg] Error in executeOneRowUpdate method. Executing query `" + query + "`.", error);
      logger.error("[DBUtilsPg] Query params: " + JSON.stringify(params));
      throw new Error(error.message);
    }
  );
}

function executeRowsUpdate(fileName, query, params, rowMode) {
  return executeQuery(fileName, query, params, rowMode).then(
    function (results) {
      return results.rowCount;//updated rows count;
    }
  ).catch(
    function (error) {
      logger.error("[DBUtilsPg] Error in executeRowsUpdate method. Executing query `" + query + "`.", error);
      logger.error("[DBUtilsPg] Query params: " + JSON.stringify(params));
      throw new Error(error.message);
    }
  );
}

function executeOneRowUpdateFetch(fileName, query, params, rowMode) {
  return executeQuery(fileName, query, params, rowMode).then(
    function (results) {
      var updatedRows = results.rowCount;

      if (updatedRows !== 1) {
        throw new Error("[DBUtilsPg] Wrong one row modification query. Rows affected: " + updatedRows);
      }
      return results.rows[0];
    }
  ).catch(
    function (error) {
      logger.error("[DBUtilsPg] Error in executeOneRowUpdateFetch method. Executing query `" + query + "`.", error);
      logger.error("[DBUtilsPg] Query params: " + JSON.stringify(params));
      throw new Error(error.message);
    }
  );
}

function executeQueryFetchRows(fileName, query, params, rowMode) {
  return executeQuery(fileName, query, params, rowMode).then(
    function (results) {
      return results.rows;
    }
  ).catch(
    function (error) {
      logger.error("[DBUtilsPg] Error in executeQueryFetchRows method. Executing query `" + query + "`.", error);
      logger.error("[DBUtilsPg] Query params: " + JSON.stringify(params));
      throw new Error(error.message);
    }
  );
}



function executeQueryFetchOneRow(fileName, query, params, rowMode) {
  return executeQuery(fileName, query, params, rowMode).then(
    function (results) {
      if (results.rows.length !== 1) {
        throw new Error("[DBUtilsPg] Error in executeQueryFetchOneRow method. Executing query `" + query + "` did not return exactly one row!");
      }
      return results.rows[0];
    }
  ).catch(
    function (error) {
      logger.error("[DBUtilsPg] Error in executeQueryFetchOneRow method. Executing query `" + query + "`.", error);
      logger.error("[DBUtilsPg] Query params: " + JSON.stringify(params));
      throw new Error(error.message);
    }
  );
}

module.exports = {
  createPgPool: createPgPool,
  getNamedQuery: getNamedQuery,
  executeQuery: executeQuery,
  executeQueryFetchOneRow: executeQueryFetchOneRow,
  executeQueryFetchRows: executeQueryFetchRows,
  executeOneRowUpdate: executeOneRowUpdate,
  executeOneRowUpdateFetch: executeOneRowUpdateFetch,
  executeRowsUpdate: executeRowsUpdate,
  executeQueryRaw: executeQueryRaw,
  executeQueryRawInsert: executeQueryRawInsert,
  executeQueryRawRowsFetch: executeQueryRawRowsFetch
};