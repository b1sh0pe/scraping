const logger = require("../../logger");
const seeMoreButtons = [
  {
    id: "showMore",
    selector: ".css-8msx5b .css-1stvaey button",
    count: 4
  }
];

const clickAllSeeMoreButtons = async (page) => {
  for (let i = 0; i < seeMoreButtons.length; i ++) {
    const button = seeMoreButtons[i];
    const elems = await page.$$(button.selector);

    //may click multiple times (since linkedin loads 5 per click)
    for (let j = 0; j < button.count; j++) {
      elems.map(async (elem) => {
        if (elem) {
          await elem.click()
            .catch(() => logger.log(`[helpers/linkedin/seeMoreButtons] Couldn't click on ${button.selector}, it's probably invisible`));
        }
      });

      if (button.count > 1) {
        //wait for more items load
        await new Promise((resolve) => {
          setTimeout(() => {
            resolve();
          }, 2000);
        });
      }
    }
  }

  return;
};

module.exports = {
  clickAllSeeMoreButtons
};

