const profileSelector = "section[id=stream-panel]";
const articleSelector = ".meteredContent";

module.exports = {
  profile: {
    selector: `${profileSelector} .css-1l4spti`,
    fields: {
      title: `${profileSelector} .css-1l4spti a > h2`,
      link: {
        selector: `${profileSelector} .css-1l4spti a`,
        attribute: "href"
      },
      description: `${profileSelector} .css-1l4spti p`,
      author: `${profileSelector} .css-1l4spti a .css-1nqbnmb span`,
      date: `${profileSelector} .css-1cp3ece .css-n1vcs8 time`
    }
  },

  article: {
    selector: `${articleSelector} .StoryBodyCompanionColumn`,
    fields: {
      text: `${articleSelector} .StoryBodyCompanionColumn p`
    }
  }
};
