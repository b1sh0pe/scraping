const logger = require("../../logger");

module.exports = async (page) => {
  const MAX_TIMES_TO_SCROLL = 25;
  const TIMEOUT_BETWEEN_SCROLLS = 1000;
  const PAGE_BOTTOM_SELECTOR_STRING = ".css-batzk0.e5u916q0";

  for (let i = 0; i < MAX_TIMES_TO_SCROLL; i++) {
    await page.evaluate(() => window.scrollBy(0, window.innerHeight));

    const hasReachedEnd = await page.waitForSelector(PAGE_BOTTOM_SELECTOR_STRING, {
      visible: true,
      timeout: TIMEOUT_BETWEEN_SCROLLS
    }).catch(() => {
      logger.log(`[helpers/linkedin/scrollToPageBottom] Scrolling to page bottom (${i + 1})`);
    });

    if (hasReachedEnd) {
      return;
    }
  }

  logger.warn("[helpers/linkedin/scrollToPageBottom] Page bottom not found.");
};
