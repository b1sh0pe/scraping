const rootPath = "../../../";
const logger = require(rootPath + "helpers/logger");
const template = require("./scrapTemplate");
const scrapSection = require("../scrapSection");
const seeMoreButtons = require("./seeMoreButtons");
const scrollToPageBottom = require("./scrollToPageBottom");

module.exports = async (page, waitTimeToScrapMs = 2000) => {
  const pageIndicator = ".css-8msx5b";
  await page.waitFor(pageIndicator, { timeout: 5000 })
    .catch(() => {
      logger.error("[scrapDashboard] Error in scrapDashboard method. Dashboard selector was not found.");
    });

  logger.log("[scrapDashboard] Scrolling page to the bottom.");
  await scrollToPageBottom(page);

  if (waitTimeToScrapMs) {
    logger.log("[scrapDashboard] Applying 1st delay.");
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, waitTimeToScrapMs / 2);
    });
  }

  logger.log("[scrapDashboard] Clicking on see more buttons.");
  await seeMoreButtons.clickAllSeeMoreButtons(page);

  if (waitTimeToScrapMs) {
    logger.log("[scrapDashboard] Applying 2nd delay.");
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, waitTimeToScrapMs / 2);
    });
  }

  const articles = await scrapSection(page, template.profile);

  return articles;
};
