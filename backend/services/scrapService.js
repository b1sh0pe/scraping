const rootPath = "../";
const config = require(rootPath + "config");
const logger = require(rootPath + "helpers/logger");
const puppeteerUtils = require(rootPath + "helpers/puppeteerUtils");
const scrapDashboard = require(rootPath + "helpers/scrapingUtils/newYorkTimes/scrapDashboard");
const scrapArticle = require(rootPath + "helpers/scrapingUtils/newYorkTimes/scrapArticle");


async function scrapArticles(page, url) {
  await puppeteerUtils.goTo(page, url, {ignoreDestination: true});
  const articlesDetails = await scrapDashboard(page);
  return articlesDetails;
}

async function scrapOneArticle(page, url) {
  await puppeteerUtils.goTo(page, url, {ignoreDestination: true});
  const articleDetails = await scrapArticle(page);
  return articleDetails;
}

async function getArticles(newYorkTimesLink) {
  if (typeof newYorkTimesLink !== "string" || !newYorkTimesLink.startsWith("http")) {
    logger.error(`[newYorkTimes] Error in getLinkedInProfileInfo method. Invalid New York Times url.\n
                  [newYorkTimes] New York Times url: ${newYorkTimesLink}`);
    return Promise.reject(false);
  }

  logger.log("Getting data from '" + newYorkTimesLink + "'...");
  const browser = await puppeteerUtils.runBrowser({headless: false});
  const page = await puppeteerUtils.createPage(browser, config.cookiesFile);
  return scrapArticles(page, newYorkTimesLink).then(
    function (personDetails) {
      browser.close();
      return personDetails;
    }
  );
}

async function getArticleData(articleLink) {
  if (typeof articleLink !== "string" || !articleLink.startsWith("http")) {
    logger.error(`[newYorkTimes] Error in getArticleData method. Invalid article url.\n
                  [newYorkTimes] Article url: ${articleLink}`);
    return Promise.reject(false);
  }

  logger.log("Getting data from '" + articleLink + "'...");
  const browser = await puppeteerUtils.runBrowser({headless: false});
  const page = await puppeteerUtils.createPage(browser, config.cookiesFile);
  return scrapOneArticle(page, articleLink).then(
    function (articleDetails) {
      browser.close();
      return articleDetails;
    }
  );
}

module.exports = {
  getArticles,
  getArticleData
};
