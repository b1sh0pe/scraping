const natural = require("natural");
const classifier = new natural.BayesClassifier();

module.exports.trainCounter = 0;

module.exports.trainModel = (articleDescription, likeOrDislike) => {
  if (!articleDescription || !likeOrDislike) {
    return;
  }

  classifier.addDocument(articleDescription, likeOrDislike);
  module.exports.trainCounter++;
  if (module.exports.trainCounter >= 5) {
    classifier.train();
  }
};

module.exports.classify = (articleDescription, result) => {
  const prediction = classifier.classify(articleDescription);
  if (result && result !== prediction) {
    module.exports.trainModel(articleDescription, result);
    return "We will count further your interests.";
  }

  return prediction;
};

