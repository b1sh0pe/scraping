const fs = require("fs");
const glob = require("glob");
const ioredis = require("ioredis");
const { GraphQLDate, GraphQLTime, GraphQLDateTime } = require("graphql-iso-date");
const { RedisPubSub } = require("graphql-redis-subscriptions");
const { makeExecutableSchema } = require("graphql-tools");
const { GraphQLJSON } = require("graphql-type-json");
const resolvers = require("./resolvers");

const REDIS_URL = process.env.REDIS_URL || "redis://localhost:6379";
const redis = new ioredis(REDIS_URL);

const files = glob.sync(`${__dirname}/**/*.gql`);
let typeDefs = "";
files.forEach(f => {
  typeDefs = typeDefs.concat(fs.readFileSync(f).toString());
});

module.exports = {
  pubsub: new RedisPubSub({ publisher: redis, subscriber: redis }),
  resolvers,
  scalars: { GraphQLDate, GraphQLDateTime, GraphQLTime, GraphQLJSON },
  schema: makeExecutableSchema({typeDefs, resolvers}),
  typeDefs
};
