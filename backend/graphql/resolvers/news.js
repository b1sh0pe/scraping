const MongoConnect = require("../../mongo-connect");
const { classify, trainModel } = require("../../services/mlService");
let trainCounter = require("../../services/mlService").trainCounter;

module.exports.Query = {
  async getNews() {
    const client = await MongoConnect.open();
    const db = client.db("SCRAPING");

    const news = db.collection("news");
    const news_status = db.collection("news_status");

    const newsWithStatus = await news_status.find().toArray();
    const newsWithoutStatus = await news.find().toArray();

    let notFilteredNews = newsWithoutStatus.filter(article => !newsWithStatus.some(elem => elem.articleId === article._id.toString()));

    if (trainCounter >= 5) {
      notFilteredNews = notFilteredNews.map(elem => {
        return Object.assign(elem, { suggestion: classify(elem.description) });
      });
    }

    return notFilteredNews;
  },
};

module.exports.Mutation = {
  async likeDislikeNews(parent, args, context, info) { // eslint-disable-line no-unused-vars
    const client = await MongoConnect.open();
    const db = client.db("SCRAPING");
    const article = args["article"];

    const news_status = db.collection("news_status");
    await news_status.updateOne({
      articleId: article._id
    }, {
      $set: {
        status: article.status,
        articleId: article._id
      }
    }, { upsert: true });

    const articleStatus = article.status ? "like" : "dislike";

    if (trainCounter >= 5) {
      classify(article.description, articleStatus);
    } else {
      trainModel(article.description, articleStatus);
    }

    trainCounter = require("../../services/mlService").trainCounter;

    const insertedDocument = await news_status.findOne(
      {
        articleId: article._id
      }
    );

    return insertedDocument;
  }
};