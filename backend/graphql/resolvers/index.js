//const { pubsub } = require("../index");
const { Query: newsQuery, Mutation: newsMutation } = require("./news");

module.exports = {
  Query: {
    ...newsQuery
  },
  Mutation: {
    ...newsMutation
  },
  // Subscription: {
  //   subscriptionExample: {
  //     subscribe: () => pubsub.asyncIterator('subscriptionExample')
  //   }
  // },
};
