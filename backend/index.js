const express = require("express");
const nuxt = require("nuxt");
const http = require("http");
const cors = require("cors");
const favicon = require("serve-favicon");
const compression = require("compression");
const { ApolloServer } = require("apollo-server-express");
const { makeExecutableSchema } = require("graphql-tools");
const { typeDefs, resolvers } = require("./graphql");

const rootPath = "./";
const config = require(rootPath + "config");
const logger = require(rootPath + "helpers/logger");

const port = config.port;
const app = express();
const httpServer = http.createServer(app);

app.set("trust proxy", true);
app.set("etag", false);

// Switch off the default "X-Powered-By: Express" header
app.disable("x-powered-by");

// Compress all requests
app.use(compression(config.compressionOptions));

/**************** CORS ******************/

const corsOptions = {
  credentials: true,
  origin: process.env.CORS_ORIGIN || "*"
};
app.use(cors(corsOptions));
app.options("*", cors(corsOptions));

/***************************************/

/**************** GRAPH QL *************/

const schema = makeExecutableSchema({ typeDefs, resolvers });

const server = new ApolloServer({
  schema
});
server.applyMiddleware({ app, cors: false });
server.installSubscriptionHandlers(httpServer);

/**************************************/


// Node.js middleware for serving a favicon
// to exclude these requests from your logs by using this middleware before your logger middleware
const publicDirPath = config.projectDir + "/../frontend/static";
app.use(favicon(publicDirPath + "/favicon.png"));

// Add X-Response-Time header to all responses
const mode = config.mode;
if (mode === "development") {
  logger.log("[index] X-Response-Time header added.");
  app.use(require("response-time")());
}

// Add request logger middleware
// Log every request to log/access.log file (ip, date, time, path, userAgent)
//if (mode === "development") {
logger.log("[index] Request logger middleware (ip, date, time, path, userAgent) added.");
require(rootPath + "middlewares/logger").initLogger(app);
//}

const nuxtPort = config.nuxtPort;
const isSinglePortMode = !nuxtPort || port === nuxtPort;

// Add CORS support for not single port mode
const tokenNameHeader = config.tokenNameHeader;
const appVersionNameHeader = config.appVersionNameHeader;
if (!isSinglePortMode) {
  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type, Content-Length, " + tokenNameHeader + ", " + appVersionNameHeader);
    res.header("Access-Control-Expose-Headers", appVersionNameHeader);
    if (req.method === "OPTIONS") {
      res.sendStatus(200);
    } else {
      next();
    }
  });
}

// Add application version to the headers to be able to reload app on client side
//const appVersion = config.appVersion;
const appRevision = config.appRevision;
app.use((req, res, next) => {
  res.header(appVersionNameHeader, appRevision);
  const appRequestVersion = req.headers[appVersionNameHeader];
  if (appRequestVersion && appRequestVersion !== appRevision) {
    res.sendStatus(205);// Reset content status //equivalent to res.status(205).send("")
  } else {
    next();
  }
});

// Following code just add Nuxt support for backend (frontend pages renderer)
if (isSinglePortMode) {
  app.set("port", port);

  // Middlewares are imported here.
  //middlewares(app);

  // Import and set Nuxt.js options
  let configNuxt = require("../frontend/nuxt.config.js");
  configNuxt.dev = mode !== "production";
  configNuxt.buildDir = config.nuxtBuildDir;

  // Init Nuxt.js
  //const nuxt = require("nuxt");
  const nuxtInstance = new nuxt.Nuxt(configNuxt);

  // Build only in dev mode
  if (configNuxt.dev) {
    new nuxt.Builder(nuxtInstance).build();
  }

  // Give nuxt middleware to express
  app.use(nuxtInstance.render);
}

const hostname = config.hostname;
httpServer.listen({ port, hostname }, err => {
  if (err) {
    throw new Error(err);
  }

  logger.log("[index] The server is running at " + hostname + ":" + port + "/ in " + mode + " mode.");
});

function serverGracefulShutdownHandler() {
  logger.info("[index] Stopping server on port " + port);

  // Force shutdown timeout
  const gracefulTimeout = setTimeout(
    function () {
      logger.info("[index] Server on port " + port + " stopped forcefully.");
      process.exit(1);
    },
    config.gracefulShutdownTimeout
  );

  server.close(
    function (error) {
      if (error) {
        logger.error("[index] Problem with server graceful shutdown!", error);
      } else {
        logger.info("[index] Server on port " + port + " stopped gracefully.");
      }

      clearTimeout(gracefulTimeout);
      process.exit(0);
    }
  );
}

// Log proces exit event
process.on("exit", function (exitCode) {
  logger.info("[index] Process exit with code: " + exitCode);
});

process.on("SIGTERM", serverGracefulShutdownHandler);
process.on("SIGINT", serverGracefulShutdownHandler);
process.on("SIGHUP", serverGracefulShutdownHandler);
process.on("message",
  function (msg) {
    if (msg === "shutdown") {
      serverGracefulShutdownHandler();
    }
  }
);

// Log proces exit event
process.on("exit", function (exitCode) {
  logger.info("[index] Process exit with code: " + exitCode);
});

// Catch and log process errors and warnings
process.on("unhandledRejection", function (error) {
  logger.error("[index] Unhandled Rejection", error);
  serverGracefulShutdownHandler();
  //process.exit(1);
});

process.on("unhandledException", function (error) {
  logger.error("[index] Unhandled Exception", error);
  serverGracefulShutdownHandler();
  //process.exit(1);
});

process.on("warning", function (error) {
  logger.warning("[index] Warning detected", error);
  serverGracefulShutdownHandler();
  //process.exit(1);
});


// TODO take a look for better part. Put it here to not miss.
// const express = require('express')
// const consola = require('consola')
// const { Nuxt, Builder } = require('nuxt')
// const app = express()

// // Import and Set Nuxt.js options
// const config = require('../nuxt.config.js')
// config.dev = !(process.env.NODE_ENV === 'production')

// async function start() {
//   // Init Nuxt.js
//   const nuxt = new Nuxt(config)

//   const { host, port } = nuxt.options.server

//   // Build only in dev mode
//   if (config.dev) {
//     const builder = new Builder(nuxt)
//     await builder.build()
//   } else {
//     await nuxt.ready()
//   }

//   // Give nuxt middleware to express
//   app.use(nuxt.render)

//   // Listen the server
//   app.listen(port, host)
//   consola.ready({
//     message: `Server listening on http://${host}:${port}`,
//     badge: true
//   })
// }
// start()