const mongoClient = require("mongodb").MongoClient;
const dbUrl = "mongodb://localhost:27017";

function open() {
  return mongoClient.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(
    (db) => {
      if (db) {
        return db;
      }

      return Promise.reject(db);
    }
  );
}

function close(db) {
  if (db) {
    db.close();
  }
}

module.exports = {
  open: open,
  close: close
};