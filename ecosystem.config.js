const appName = "scraping";
const env_shared = {
  AJAX_REQUEST_NAME_HEADER: "X-Requested-With",
  AJAX_REQUEST_VALUE_HEADER: "XMLHttpRequest",

  APP_VERSION_NAME_HEADER: "x-app-version",
  APP_VERSION: "0.0.1",
  APP_REVISION: "0.0.1",
  APP_NAME: appName
};

const projectDir = __dirname;
const env_develop = Object.assign(
  {
    // Backend and frontend configuration (passed to nuxt.config.js)
    NODE_ENV: "development",
    PORT: 3121,
    NUXT_HOST: "0.0.0.0",// Used by staring nuxt server via nuxt.js script
    NUXT_PORT: 3122,// Used by staring nuxt server via nuxt.js script
    NUXT_BUILD_DIR: projectDir + "/build/nuxt-development",
    APP_NAME: appName + "-develop",
    PUBLIC_APP_PORT: 3122,

    //# Database connections
    DB_TYPE: "postgres",
    DB_HOST: "192.168.1.50",
    DB_PORT: 5432,
    DB_USER: "postgres",
    DB_PASSWORD: "postgres",
    DB_NAME: "SCRAPING"

    //JWT_SECRET: "superSecret"
  },
  JSON.parse(JSON.stringify(env_shared))
);

const env_production = Object.assign(
  {
    // Backend and frontend configuration (passed to nuxt.config.js)
    NODE_ENV: "production",
    PORT: 3120,
    NUXT_HOST: "0.0.0.0",
    NUXT_PORT: 3120,
    NUXT_BUILD_DIR: projectDir + "/build/nuxt-production",
    PUBLIC_APP_PORT: 80,

    //# Database connections
    DB_TYPE: "postgres",
    DB_HOST: "127.0.0.1",//"192.168.1.50",
    DB_PORT: 5432,
    DB_USER: "postgres",
    DB_PASSWORD: "postgres",
    DB_NAME: "SCRAPING"

    //JWT_SECRET: "superSecret",
  },
  JSON.parse(JSON.stringify(env_shared))
);

module.exports = {
  apps: [
    {
      name: appName + "-frontend-dev",
      script: "./node_modules/nuxt/bin/nuxt.js",
      args: "-c ./frontend/nuxt.config.js",
      env: env_develop
    }, {
      name: appName + "-backend-dev",
      script: "./backend/index.js",
      env: env_develop,
      watch: ["backend"],
      ignore_watch: ["./backend/log/*"]
    }, {
      name: appName + "-app",
      script: "./backend/index.js",
      args: "-c ./frontend/nuxt.config.js",
      env: env_production
    }, {
      name: appName + "-app-build",
      autorestart: false,// One time run
      script: "./node_modules/nuxt/bin/nuxt.js",
      args: "build -c ./frontend/nuxt.config.js",
      env: env_production
    }
  ],
};