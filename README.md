
## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br>
Open [http://localhost:3122](http://localhost:3122) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run rabbit-mq`

Launches the Rabbit MQ listener, that will be waiting for new data after scraping the data.

### `npm run scraping`

Scrapes the news from "https://www.nytimes.com/" and sends it to Rabbit MQ.

### `npm run manual-scraping`

Runs both "npm run rabbit-mq" and "npm run scraping" in parallel (currently works only on Windows due to specification of commands)