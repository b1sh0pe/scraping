function formatMessage(module, message) {
  return "[" + module + "] " + message;
}

const logger = {

  /* eslint-disable no-console */
  log(module, message) {
    console.log(formatMessage(module, message));
  },

  error(module, message) {
    console.error(formatMessage(module, message));
  }
  /* eslint-enable no-console */
};

module.exports = logger;