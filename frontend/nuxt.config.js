import constants from "./constants";
const pkg = require("../package");

module.exports = {
  mode: "spa",//"universal"
  srcDir: __dirname,//srcDir option is required if you want to place nuxt in a sub folder

  buildDir: process.env.NUXT_BUILD_DIR,

  // The folloving config will replace all process.env.baseUrl with values passed in env variables to compile frontend with this values
  env: {
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,
    NUXT_HOST: process.env.NUXT_HOST,
    NUXT_PORT: process.env.NUXT_PORT,
    API_BASE_URL: constants.API_BASE_URL,
    PUBLIC_APP_PORT: process.env.PUBLIC_APP_PORT,

    FRONTEND_ROLLBAR_TOKEN: process.env.FRONTEND_ROLLBAR_TOKEN,

    AJAX_REQUEST_NAME_HEADER: process.env.AJAX_REQUEST_NAME_HEADER,
    AJAX_REQUEST_VALUE_HEADER: process.env.AJAX_REQUEST_VALUE_HEADER,
    APP_VERSION_NAME_HEADER: process.env.APP_VERSION_NAME_HEADER,

    APP_VERSION: process.env.APP_VERSION,
    APP_REVISION: process.env.APP_REVISION,
    APP_NAME: process.env.APP_NAME
  },

  head: {
    title: pkg.name,
    meta: [
      {
        charset: "utf-8"
      }, {
        name: "viewport",
        content: "width=device-width, initial-scale=1"//, user-scalable=no
      }, {
        hid: "description",
        name: "description",
        content: pkg.description
      }, {
        "http-equiv": "X-UA-Compatible",
        content: "IE=edge"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      }
    ]
  },

  /*
   ** Global CSS
   */
  css: [
    {
      src: "~/scss/main.scss",
      lang: "scss"
    }
  ],

  noscript: [
    {
      innerHTML: "Your browser doesn`t have JavaScript support!"
    }
  ],

  modules: [
    "@nuxtjs/apollo"
    // Doc: https://axios.nuxtjs.org/usage
    // "@nuxtjs/axios",
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    // "@nuxtjs/bulma",
    // "@nuxtjs/pwa"
  ],

  apollo: {
    includeNodeModules: true,
    clientConfigs: {
      default: {
        httpEndpoint: constants.API_BASE_URL
      },
    }
  },

  /*
  ** Customize the progress bar color
  */
  loading: {
    color: "#E7931D",
    height: "3px"
  },

  router: {
    linkActiveClass: "is-active"
  },

  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    }
  },

  plugins: []
};