module.exports = {
	mode: 'spa',
	head: {title: 'scraping'}, // Headers of the page
	loading: false, // Disable default loading bar
	build: {
		extend (config, { isDev, isClient }) {
			//Extend only webpack config for client-bundle
			if (isClient) { config.target = 'electron-renderer' }
		}
	},

	head: {
    title: "scraping",
    meta: [
      {
        charset: "utf-8"
      }, {
        name: "viewport",
        content: "width=device-width, initial-scale=1"//, user-scalable=no
      }, {
        hid: "description",
        name: "description",
        content: "Something here..."
      }, {
        "http-equiv": "X-UA-Compatible",
        content: "IE=edge"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      }
    ]
  },

	env: {
    NODE_ENV: "development",
    PORT: 3121,
    NUXT_HOST: "0.0.0.0",// Used by staring nuxt server via nuxt.js script
    NUXT_PORT: 3122,// Used by staring nuxt server via nuxt.js script
    API_BASE_URL: "http://localhost:3121/graphql",
    PUBLIC_APP_PORT: 3122,

    AJAX_REQUEST_NAME_HEADER: "X-Requested-With",
  	AJAX_REQUEST_VALUE_HEADER: "XMLHttpRequest",
  	APP_VERSION_NAME_HEADER: "x-app-version",

    APP_VERSION: "0.0.1",
	  APP_REVISION: "0.0.1",
	  APP_NAME: "scraping"
  },

	dev: process.env.NODE_ENV === 'DEV',
	css: [
		'@/assets/css/global.css'
	],

	modules: [
    "@nuxtjs/apollo"
    // Doc: https://axios.nuxtjs.org/usage
    // "@nuxtjs/axios",
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    // "@nuxtjs/bulma",
    // "@nuxtjs/pwa"
  ],

  noscript: [
    {
      innerHTML: "Your browser doesn`t have JavaScript support!"
    }
  ],

  apollo: {
    includeNodeModules: true,
    clientConfigs: {
      default: {
        httpEndpoint: "http://localhost:3121/graphql"
      },
    }
  },
}
