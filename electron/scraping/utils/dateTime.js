import moment from "moment";
const defaultDateFormat = "DD-MM-YYYY";
const defaultDateTimeFormat = "DD-MM-YYYY HH:mm:ss";

function unixTimestampToDate(dateUnixTimestamp) {
  return moment.utc(dateUnixTimestamp).format(defaultDateFormat);
}

function unixTimestampToDateTime(dateUnixTimestamp) {
  return moment.utc(dateUnixTimestamp).format(defaultDateTimeFormat);
}

function changeDateFormat(dateString, currentDateTimeFormat, dateFormat) {
  return moment(dateString, currentDateTimeFormat).format(dateFormat);
}

// function dateToUnixTimestamp(dateString) {// Unix timestamp
//   return moment.utc(dateString, defaultDateFormat, true).valueOf();// or +moment.utc(dateString, defaultDateFormat);
//   //return moment(dateString, defaultDateFormat, true).valueOf();
// }

// function durationSecondsToString(durationSeconds) {
//   //return moment.utc(durationSeconds * 1000,).format("h[h] m[m]");
//   if (!durationSeconds || durationSeconds < 0) {
//     //console.error("[helpers] Error in durationSecondsToString method. Invalid duration: " + durationSeconds);
//     return "Invalid duration";
//   }
//   const hours = Math.floor(durationSeconds / 3600);
//   const minutes = Math.floor(durationSeconds % 3600 / 60);
//   let formattedDuration = "";
//   if (hours > 0) {
//     formattedDuration = hours + "h";
//   }
//   if (minutes > 0) {
//     formattedDuration = (formattedDuration ? formattedDuration +  " " : "") + minutes + "m";
//   }
//   return formattedDuration;
// }

// function durationStringToSeconds(durationString) {
//   //return moment.utc(dateString, "h[h] m[m]").valueOf();
//   let match = durationString.match(/(\d+)[H|h]/);
//   const hours = match !== null ? match[1] : null;
//   match = durationString.match(/(\d+)[M|m]/);
//   const minutes = match !== null ? match[1] : null;
//   if (!hours && !minutes) {
//     //console.error("[helpers] Error in durationStringToSeconds method. Invalid duration: " + durationString);
//     return NaN;
//   }
//   return hours * 3600 + minutes * 60;
// }

// function getCurrentDateTimestamp() {
//   return dateToUnixTimestamp(unixTimestampToDate(new Date().getTime()));
// }

// function getTimeFromNow(dateUnixTimestamp) {
//   const today = new Date();
//   // If you pass true, you can get the value without the suffix.
//   // moment([2007, 0, 29]).fromNow();   // 4 years ago
//   // moment([2007, 0, 29]).fromNow(true); // 4 years
//   //return moment.utc(dateUnixTimestamp).fromNow();
//   return moment(dateUnixTimestamp).from(today.getTime() - today.getTimezoneOffset() * 60 * 1000);
// }

export default {
  unixTimestampToDate: unixTimestampToDate,
  unixTimestampToDateTime: unixTimestampToDateTime,
  changeDateFormat: changeDateFormat

  // dateToUnixTimestamp: dateToUnixTimestamp,

  // durationSecondsToString: durationSecondsToString,
  // durationStringToSeconds: durationStringToSeconds,

  // getCurrentDateTimestamp: getCurrentDateTimestamp,

  // getTimeFromNow: getTimeFromNow
};